#import "app_delegate.h"
#include <OpenGL/gl.h>

@implementation AppDelegate

@synthesize window;
@synthesize openGLContext;

static NSOpenGLPixelFormatAttribute glAttributes[] = {
    NSOpenGLPFAColorSize, 24,
    NSOpenGLPFAAlphaSize, 8,
    NSOpenGLPFADoubleBuffer,
    NSOpenGLPFAAccelerated,
    0
};

- (void)draw {
    NSLog(@"Drawing...");

    [self.openGLContext makeCurrentContext];

    glClearColor(1, 0, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    glColor3f(1.0f, 0.85f, 0.35f);
    glBegin(GL_TRIANGLES);
    {
        glVertex3f(  0.0,  0.6, 0.0);
        glVertex3f( -0.2, -0.3, 0.0);
        glVertex3f(  0.2, -0.3 ,0.0);
    }
    glEnd();

    [self.openGLContext flushBuffer];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    NSRect frame = NSMakeRect(0, 0, 200, 200);

    self.window = [[[NSWindow alloc]
        initWithContentRect:frame
        //styleMask:NSBorderlessWindowMask
        styleMask: NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
        backing:NSBackingStoreBuffered
        defer:NO] autorelease];
    [self.window makeKeyAndOrderFront:nil];

    NSOpenGLPixelFormat *pixelFormat
        = [[NSOpenGLPixelFormat alloc] initWithAttributes:glAttributes];
    self.openGLContext = [[NSOpenGLContext alloc]
        initWithFormat:pixelFormat shareContext:nil];
    [self.openGLContext setView:[self.window contentView]];

    [NSTimer
        scheduledTimerWithTimeInterval:.1
        target:self
        selector:@selector(draw)
        userInfo:nil
        repeats:YES];

    [self.window orderFrontRegardless];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)_app {
    return YES;
}

@end
