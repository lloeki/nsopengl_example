// https://developer.apple.com/library/archive/documentation/GraphicsImaging/Conceptual/OpenGL-MacProgGuide/opengl_drawing/opengl_drawing.html#//apple_ref/doc/uid/TP40001987-CH404-SW8

#import "nsopengl_example.h"

int main(int argc, const char * argv[])
{
    NSRect frame = NSMakeRect(0, 0, 200, 200);

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    [NSApplication sharedApplication];

    NSWindow * window = [[[NSWindow alloc]
        initWithContentRect:frame
        styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
        backing:NSBackingStoreBuffered
        defer:NO] autorelease];
    [window makeKeyAndOrderFront:nil];

    NSWindowController * windowController = [[[NSWindowController alloc]
        initWithWindow:window] autorelease];

    NSOpenGLPixelFormat *pixelFormat = [OpenGLView defaultPixelFormat];
    OpenGLView *openGLView = [[OpenGLView alloc] init];

    [openGLView initWithFrame: frame pixelFormat:pixelFormat];
    [openGLView setOpenGLContext: [[NSOpenGLContext alloc]
        initWithFormat:pixelFormat shareContext:nil]];
    [openGLView.openGLContext setView:[window contentView]];

    [NSTimer
        scheduledTimerWithTimeInterval:.1
        target:openGLView
        selector:@selector(drawRect:)
        userInfo:nil
        repeats:YES];

    // TODO: Create app delegate to handle system events.
    // TODO: Create menus (especially Quit!)

    [window orderFrontRegardless];
    [NSApp run];

    [pool drain];

    return 0;
}
