#import "app_delegate.h"

#import <Cocoa/Cocoa.h>

int main(int argc, char **argv) {
    [NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    [NSApp run];
}
