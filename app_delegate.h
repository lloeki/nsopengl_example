#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate> {
    NSWindow *window;
    NSOpenGLContext *openGLContext;
}

@property (assign) NSWindow *window;
@property (assign) NSOpenGLContext *openGLContext;

- (void)draw;

@end
