all: example

example: *.h *.m
	clang -DGL_SILENCE_DEPRECATION -Wl,-framework,OpenGL -Wl,-framework,Cocoa nsopengl_example.m nsopengl_example_main.m -o example

app_delegate: *.h *.m
	clang -DGL_SILENCE_DEPRECATION -Wl,-framework,OpenGL -Wl,-framework,Cocoa app_delegate.m app_delegate_main.m -o app_delegate

clean:
	rm -rf example app_delegate

PHONY: clean
