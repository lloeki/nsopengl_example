#import "nsopengl_example.h"
#include <OpenGL/gl.h>

@implementation OpenGLView

static NSOpenGLPixelFormatAttribute glAttributes[] = {
    NSOpenGLPFAColorSize, 24,
    NSOpenGLPFAAlphaSize, 8,
    NSOpenGLPFADoubleBuffer,
    NSOpenGLPFAAccelerated,
    0
};

- (void)setOpenGLContext:(NSOpenGLContext*)context {
    _openGLContext = context;
}

- (NSOpenGLContext*)openGLContext {
    return _openGLContext;
}

- (void)setPixelFormat:(NSOpenGLPixelFormat*)pixelFormat {
    _pixelFormat = pixelFormat;
}

- (NSOpenGLPixelFormat*)pixelFormat {
    return _pixelFormat;
}

+ (NSOpenGLPixelFormat*) defaultPixelFormat {
    NSOpenGLPixelFormat *pixelFormat
        = [[NSOpenGLPixelFormat alloc] initWithAttributes:glAttributes];
    return pixelFormat;
}

- (void)clearGLContext {
}

- (void)prepareOpenGL {
}

- (void)update {
    [_openGLContext update];
}

- (id)initWithFrame:(NSRect)frameRect pixelFormat:(NSOpenGLPixelFormat*)format
{
    self = [super initWithFrame:frameRect];
    if (self != nil) {
        _pixelFormat   = [format retain];
    [[NSNotificationCenter defaultCenter] addObserver:self
                     selector:@selector(_surfaceNeedsUpdate:)
                     name:NSViewGlobalFrameDidChangeNotification
                     object:self];
    }
    return self;
}

- (void) _surfaceNeedsUpdate:(NSNotification*)notification
{
   [self update];
}

- (void)lockFocus
{
    NSOpenGLContext* context = [self openGLContext];

    [super lockFocus];
    if ([context view] != self) {
        [context setView:self];
    }
    [context makeCurrentContext];
}

-(void) drawRect: (NSRect) bounds
{
    NSOpenGLContext* context = [self openGLContext];

    [context makeCurrentContext];

    NSLog(@"Drawing...");

    glClearColor(1, 0, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    drawAnObject();
    //glFlush();

    [context flushBuffer];
}

static void drawAnObject ()
{
    glColor3f(1.0f, 0.85f, 0.35f);
    glBegin(GL_TRIANGLES);
    {
        glVertex3f(  0.0,  0.6, 0.0);
        glVertex3f( -0.2, -0.3, 0.0);
        glVertex3f(  0.2, -0.3 ,0.0);
    }
    glEnd();
}

-(void) viewDidMoveToWindow
{
    [super viewDidMoveToWindow];
    if ([self window] == nil) {
        NSOpenGLContext* context = [self openGLContext];
        [context clearDrawable];
    }
}
@end
